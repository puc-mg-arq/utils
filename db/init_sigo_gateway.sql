create schema sigo_gateway

-- auto-generated definition
    create table routers
    (
        id           serial not null
            constraint sigo_token_pkey
                primary key,
        config        jsonb,
        date_created timestamp default timezone('BRT'::text, now())
    )

    create
        unique index sigo_routers_id_uindex
        on routers (id);


INSERT INTO sigo_gateway.routers (config)
VALUES ('{
  "sigo-auth": {
    "host-redirect": "http://192.168.0.100:5000",
    "app-context": "/sigo-auth"
  },
  "login": {
    "host-redirect": "http://192.168.0.100:5000",
    "app-context": "/sigo-auth/login"
  },
  "sigo-auth-validate": {
    "host-redirect": "http://192.168.0.100:5000",
    "app-context": "/sigo-auth/token/valid"
  },
  "sigo-consult_asses": {
    "host-redirect": "http://192.168.0.100:5002",
    "app-context": "/sigo-consult_asses"
  }
}');